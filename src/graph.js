//$Id$
var graph = {
    template: `<v-container class="pa-8" fill-height fluid>
     <v-card flat width="100%" height="100%" style="overflow:auto;">
     <v-card-title>
            Number of mutual funds under each category
     </v-card-title>
     <v-container class="pa-8"  > 
     <svg v-bind:width="settings.width" v-bind:height="settings.height" viewBox="0 0 1300 1300"> 
     <g transform="translate(100,0)">    
       <g>
         <path fill="none" v-for="link in links" class="link"  v-bind:d="link.d" v-bind:style="link.style"></path>
       </g>
       <g>
         <g class="node" v-on:click="select(index, node)" v-for="(node, index) in nodes" v-bind:style="node.style" v-bind:class="[node.className, {'highlight': node.highlight}]">
         <circle v-bind:r="node.r" v-bind:style="{'fill': index == selected ? '#ff0000' : '#bfbfbf'}"></circle>
          <text v-bind:dx="node.textpos.x" v-bind:dy="node.textpos.y" v-bind:style="node.textStyle">{{ node.id }}</text>
         </g>
       </g>
      </g> 
     </svg>
     </v-container>
     </v-card>
    </v-container>`,
    data() {
      return {
         data :[],
         selected:"",
         settings: {
          strokeColor: "#19B5FF",
          width: 1400,
          height: 1000,
        }
      };
    },
    mounted(){
      this.getData();
    },
    computed: {
      transformedData() {
        var nested_data = d3.nest()
.key(function(d) { return d.fund_type; }).sortKeys(d3.ascending)
.key(function(d) { return d.fund_category; })
.rollup(function(leaves) { return leaves.length; })
.entries(this.data);
        return {
          key: 'MutualFunds',
          values: nested_data.map(data => ({
            ...data,
            parent: 'MutualFunds'
          }))
        }
      },      
      root: function() {        
        var that = this;
        if (this.transformedData) {
          const rootHierarchy =d3.hierarchy(this.transformedData,function(d){ return d.values })
          .sum(d => d.year_1)
          .sort((a, b) => {
            return b.value - a.value
          });
          return this.tree(rootHierarchy);
        }
      },      
      tree: function() {
        return d3
          .cluster()
          .size([this.settings.height, this.settings.width - 500]);
      },      
      nodes: function() {
        var that = this;
        if (this.root) {
          return this.root.descendants().map(function(d) {
            return {
              id: d.data.key+(d.data.value?(" ( "+d.data.value+" )"):""),
              r: 2.5,
              className: "node" +
                (d.children ? " node--internal" : " node--leaf"),
              text: d.name,
              style: {
                transform: "translate(" + d.y + "px," + d.x + "px)"
              },
              textpos: {
                x: d.children ? -8 : 8,
                y: 3
              },
              textStyle: {
                textAnchor: d.children ? "end" : "start"
              }
            };
          });
        }
      },      
      links: function() {
        var that = this;
        if (this.root) {
          return this.root.descendants().slice(1).map(function(d) {
            return {
              id: d.data.key,
              d: "M" + d.y + "," + d.x + "C" + (d.parent.y + 100) + "," + d.x + " " + (d.parent.y + 100) + "," + d.parent.x + " " + d.parent.y + "," + d.parent.x,              
              style: {
                stroke: that.settings.strokeColor,
              }
            };
          });
        }
      } 
    },
    methods: {
      getData(){
        axios.get("https://api.kuvera.in/api/v3/funds.json").then( (response) => {
          this.data=response.data;
          this.data=this.data.filter(function(d){  return d.plan;    });
          this.data.forEach((d)=>{
              d.year_1=d.returns.year_1;d.year_3=d.returns.year_3;  
          });
        })
      }
    },
    select(index, node) {
      this.selected = index;
    }
    
  }; //No I18N
  