//$Id$
var details = {
    template: `<v-container class="pa-8" fill-height fluid>
     <v-card flat width="100%" height="100%">
     <v-container class="pa-8"> 
     <v-btn color="primary"  text @click="goBack()" > <v-icon>arrow_back</v-icon> Back</v-btn>
     <v-container>
        <div class="title" v-text="item.name"></div>
        <div><v-chip label color="primary"  v-text="item.category"></v-chip><v-chip color="secondary" label class="ml-4" v-text="item.plan"></v-chip><v-chip color="info" label class="ml-4" v-text="item.fund_category"></v-chip></div>
        <div class="ma-6">
        <blockquote class="blockquote d-inline" v-text="item.investment_objective"></blockquote>
        <div class="body-2 d-inline" style="position:relative;bottom:-20px;"> - {{item.fund_manager}}</div>
        </div>
        <div >Start Date : <b>{{item.start_date}}</b></div>
        <div>Year-1 Returns : <b>{{item.returns.year_1}}</b></div>
        <div>Year-3 Returns : <b>{{item.returns.year_3}}</b></div>
        <div>Year-5 Returns : <b>{{item.returns.year_5}}</b></div>
        <div>inception : <b>{{item.returns.inception}}</b></div>
        <div>Date : <b>{{item.returns.date}}</b></div>
        <div class="mt-4"> View more details at  <a target="_blank"  :href="item.detail_info">{{item.detail_info}}</a> </div>
     </v-container>   
     </v-container>
     </v-card>
    </v-container>`,
    data() {
      return {
        item: {},
      };
    },
    props:["code"],
    mounted(){
      this.getData();
    },
    methods: {
      getData(){
        axios.get("https://api.kuvera.in/api/v3/funds/"+this.$route.params.code+".json").then( (response) => {
          this.item=response.data[0];
        })
      },
      goBack(){
            router.go(-1);
      }
    }
  }; //No I18N
  