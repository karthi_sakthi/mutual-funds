//$Id$
var layout = {
  template: `<v-app style="background:#f7f8fd">
      <v-navigation-drawer
              persistent
              mini-variant
              mini-variant-width="120"
              fixed
              floating
              app
              class="text-center"
      >
          <div style="height:calc( 100% - 100px );">
                      <div
                              :key="item.icon"
                              v-for="(item,index) in items" 
                              class="mt-10  mainNav"
                              :class="selectedIndex===index?'selectedNav':' '"		 
                              @click="selectedIndex=index;$router.push(item.link);"
                              style="cursor:pointer"
                      >
                              <v-icon size="35" :color="selectedIndex===index?'primary':'info'"  v-html="item.icon"></v-icon>
                              <div :class="selectedIndex===index?'primary--text':'info--text'" v-text="item.title" ></div>
                      </div>
          </div> 
          <v-avatar>
          <img
                    alt="Avatar"
                    src="https://media.licdn.com/dms/image/C5103AQFUmhcmU-5PVg/profile-displayphoto-shrink_100_100/0?e=1574294400&v=beta&t=0DUk1HIidXiMtC95AQahPgvbXEfaavJvQIMoy14Jcg0"
                  >
          </v-avatar>
      </v-navigation-drawer>
      <v-content>
      <router-view></router-view>
      </v-content>
  </v-app>`,
  props: ['portal'],
  data() {
    return {
      items: [
        {
          icon: 'table_chart',
          title: 'Table View',
          link:"/mutualfunds/tableview"
        },
        {
          icon: 'list',
          title: 'List View',
          link:"/mutualfunds/cardview"
        },
        {
          icon: 'line_style',
          title: 'Categories',
          link:"/mutualfunds/graphview"
        }
      ],
      selectedIndex:0,
      searching: false,
      search: ''
    };
  },
  methods: {
    
  }
}; //No I18N
