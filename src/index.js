//$Id$
var home = {
  template: `<v-app class="white">

</v-app>`,
  data() {
    return {
      loading: false,
      start:false,
      serviceName: '',
      links:["link2","link3"],
      portals:[{"name":"Somone","email":"someon@someone.com","id":"portalid","date":"12-2-2031"},{"name":"Somone","email":"someon@someone.com","id":"portalid","date":"12-2-2031"},{"name":"Somone","email":"someon@someone.com","id":"portalid","date":"12-2-2031"},{"name":"Somone","email":"someon@someone.com","id":"portalid","date":"12-2-2031"},{"name":"Somone","email":"someon@someone.com","id":"portalid","date":"12-2-2031"},{"name":"Somone","email":"someon@someone.com","id":"portalid","date":"12-2-2031"},{"name":"Somone","email":"someon@someone.com","id":"portalid","date":"12-2-2031"}]
    };
  },
  computed: {
    portals() {
      return ['Creator', 'Docs', 'Zmail', 'Show', 'Books', 'Projects'].filter(
        d => {
          return d.indexOf(this.serviceName) >= 0;
        }
      );
    }
  }
}; //No I18N
