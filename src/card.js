//$Id$
var cards = {
    template: `<v-container class="pa-8" fill-height fluid>
     <v-card flat width="100%" height="100%">
     <v-container class="pa-8"> 
       <v-row> <span class="headline"> Mutual Funds - Card View </span>  </v-row>
       <v-card flat>
       <v-card-title class="mx-0 px-0  hidden-md-and-down">
       <v-text-field
       v-model="search"
       solo
       append-icon="search"
       label="Search by name"
       single-line
       hide-details
     ></v-text-field>
         <div class="flex-grow-1"></div>
         <div class="ml-6" v-for="(filter,index) in filters" :key="filter.name">
         <div class="caption mb-3"> {{filter.name}}  </div>
         <v-select
         v-model="filter.selection"
         :items="filter.array"
         :label="filter.name"
         style="width:220px;"
         solo
         @change="filterChange(index)"
       ></v-select>
       </div> 
       </v-card-title>
       <v-card-title class="ma-0 hidden-md-and-down">
       <div class="grey--text mr-3"> Sort by :   </div>
       <v-btn-toggle v-model="sort">
       <v-btn text>
        Name
       </v-btn>
       <v-btn text>
         Category
       </v-btn>
       <v-btn text>
         Type
       </v-btn>
       <v-btn text>
         Plan
       </v-btn>
       <v-btn text>
       Year 1 returns
     </v-btn>
     <v-btn text>
     Year 3 returns
     </v-btn>
     </v-btn-toggle>
       </v-card-title>
       <v-card-title class="mx-0 px-0  hidden-lg-and-up">
       <v-text-field
       v-model="search"
       solo
       prepend-inner-icon="search"
       label="Search by name"
       single-line
       hide-details
       append-outer-icon="filter_list"
       @click:append-outer="dialog=true"
     >
     </v-text-field>
       <v-dialog v-model="dialog" fullscreen hide-overlay transition="dialog-bottom-transition">
       <v-card>
       <v-subheader> Sort </v-subheader>
       <v-btn-toggle class="ml-6" v-model="sort">
       <v-btn text>
        Name
       </v-btn>
       <v-btn text>
         Category
       </v-btn>
       <v-btn text>
         Type
       </v-btn>
       <v-btn text>
         Plan
       </v-btn>
       <v-btn text>
       Year 1 returns
     </v-btn>
     <v-btn text>
     Year 3 returns
     </v-btn>
     </v-btn-toggle>

       <v-subheader> Filters</v-subheader>
       <div class="mb-6 mx-10"  v-for="(filter,index) in filters" :key="filter.name">
       <div class="caption mb-3"> {{filter.name}}  </div>
       <v-select
       v-model="filter.selection"
       :items="filter.array"
       :label="filter.name"
       solo
       @change="filterChange(index)"
     ></v-select>
     </div>
     <v-btn class="mx-10" @click="dialog=false;">Apply</v-btn>
     </v-card>
         </v-dialog>      
       </v-card-title>
      <v-card-text style="height:600px;overflow:auto;"> 
             <div class="text-center pa-10 ma-10" v-show="funds.length===0">No records match this filter</div>
             <v-card @click="rowClick(fund)" class="pa-6 my-4"  v-for="fund in funds">
             <v-layout wrap>
             <v-flex xs12 sm6 md8 > <div class="title blue-grey--text" v-text="fund.name"> </div><div class="grey--text" v-text="fund.fund_category+' , '+fund.fund_type+' , '+fund.plan"> </div> </v-flex>
             <v-flex xs12 sm6 md4  class="text-center"><v-layout> 
             <v-layout column><v-flex class="title" v-text="fund.year_1"></v-flex><v-flex class="caption grey--text">year_1 returns</v-flex></v-layout> 
             <v-layout column><v-flex class="title" v-text="fund.year_3"></v-flex><v-flex class="caption grey--text">year_3 returns</v-flex></v-layout> 
             </v-layout>
             </v-flex>
             </v-layout>   
             </v-card>   
      </v-card-text>
     </v-card>
     </v-container>
     </v-card>
    </v-container>`,
    data() {
      return {
        page:1,
        dialog:false,
        pageCount: 0,
        items: [],
        sort:"",
        headers:[{
          text: 'Fund Name',
          align: 'left',
          value: 'name',
        },
        { text: 'Fund Category', value: 'fund_category' },
        { text: 'Fund Type', value: 'fund_type' },
        { text: 'plan', value: 'plan' },
        { text: 'Year-1 Returns', value: 'year_1' },
        { text: 'Year-3 Returns', value: 'year_3' }
        ],
        itemsPerPage:parseInt(window.innerHeight/80),
        search: '',
        filters:[{name:"Fund Type",selection:"ALL",list:new Set(),"key":"fund_type",array:[]},{name:"Fund Category",selection:"ALL",list:new Set(),"key":"fund_category",array:[]},{name:"Plan","key":"plan",selection:"ALL",array:[],list:new Set()}],
        changeTracker: true,

      };
    },
    mounted(){
      this.filters.forEach(function(d){  d.list.add("ALL")  });
      this.getData();
    },
    computed:{
      funds(){
        let arr=this.items,search=this.search;
        this.filters.forEach(function(filter){
            let selection=filter.selection==="ALL"?"":filter.selection;
            filter.list.clear(); filter.list.add("ALL");
            arr=arr.filter(function(d){ filter.list.add(d[filter.key]);  return d[filter.key].indexOf(selection)===0&&d.name.toLowerCase().indexOf(search.toLowerCase())>=0;;  });
            
        });
        this.filters.forEach(function(filter){  filter.array=Array.from(filter.list);  });
        if(this.sort>=0){
            let sortables= ["name","fund_category","fund_type","plan","year_1","year_3"];
           arr=arr.sort((a,b)=>{ 
                 if(a[sortables[this.sort]]>b[sortables[this.sort]]){
                     return -1;
                 }
                 else if(a[sortables[this.sort]]>b[sortables[this.sort]]){
                     return 1;
                 }
                 else{
                     return 0;
                 }   
            });
        }
        return arr.slice(0,100);
      },
    },
    methods: {
      getData(){
        axios.get("https://api.kuvera.in/api/v3/funds.json").then( (response) => {
          this.items=response.data;
          this.items=this.items.filter(function(d){  return d.plan;    });
          this.items.forEach((d)=>{
              this.filters.forEach(function(filter){  filter.list.add(d[filter.key]); });
              d.year_1=d.returns.year_1;d.year_3=d.returns.year_3;  
          })
          this.filters.forEach(function(filter){  filter.array=Array.from(filter.list);  });
        })
      },
      filterChange(index){
         var i=index;
         while(i<this.filters.length-1){
            i++;
            this.filters[i].selection="ALL";
         }
      },
      rowClick(row){
          router.push("details/"+row.code);
      }
    }
  }; //No I18N
  