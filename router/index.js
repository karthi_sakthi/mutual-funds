//$Id$
var routes = [
  { path: '', component: home },
  {
    path: '/mutualfunds/',
    component: layout,
    children: [
      { path: 'tableview', component: content },
      { path: 'cardview', component: cards },
      { path: 'graphview', component: graph },
      { path: 'details/:code', component: details },
    ]
  }
];   //No I18N
